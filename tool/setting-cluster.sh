#!/bin/bash

echo "Setting $(kubectl config current-context) Cluster!!"

cd $(dirname $0)

# calicoのデプロイ
kubectl apply -f ../manifest/cluster/infrastructure/calico/tigera-calico.yaml
kubectl apply -f ../manifest/cluster/infrastructure/calico/custom-resources.yaml

# metallbのデプロイ
kustomize build ../manifest/cluster/infrastructure/metallb | kubectl apply -f -

# NGINX Ingress Controllerのデプロイ
kustomize build ../manifest/cluster/infrastructure/ingress-nginx | kubectl apply -f -

# metrics-serverのデプロイ
kubectl apply -f ../manifest/cluster/infrastructure/metrics-server/components.yaml

# cert-managerのデプロイ
kubectl apply -f ../manifest/cluster/infrastructure/cert-manager/cert-manager.yaml

# argoCDのデプロイ
kustomize build ../manifest/cluster/infrastructure/argocd | kubectl apply -f -
