.PHONY: create-cluster
create-cluster:
	@bash ./tool/create-cluster.sh my-app-cluster

.PHONY: setting-cluster
setting-cluster:
	@bash ./tool/setting-cluster.sh

.PHONY: delete-cluster
delete-cluster:
	@bash ./tool/delete-cluster.sh my-app-cluster
